package com.training.charmas.app

import com.training.charmas.app.backend.getDemoApi
import org.junit.jupiter.api.Test

class Exercise {

    private val api = getDemoApi()

    /**
     * Pobierz i wyświetl listę postów.
     */
    @Test
    fun exercise1(): Unit = TODO()

    /**
     * Pobierz listę postów i wyświetl komentarze dla pierszego z nich
     */
    @Test
    fun exercise2(): Unit = TODO()

    /**
     * Pobierz listę postów wraz z komentarzami dla każdego z nich. Komentarze pobieraj sekwencyjnie.
     */
    @Test
    fun exercise3(): Unit = TODO()


    /**
     * Pobierz listę postów raz z komentarzami. Tym razem zrób to równolegle.
     *
     * Podpowiedż: List<Deffered<T>>.awaitAll() - czeka na wszystkie operacje async w kolekcji
     */
    @Test
    fun exercise4(): Unit = TODO()

    /**
     * Do postów z komentarzami pobierz dodatkowo inforamcje o autorze.
     */
    @Test
    fun exercise5(): Unit = TODO()
}
