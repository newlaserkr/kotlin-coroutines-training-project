package com.training.charmas.app

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

class Examples {

    @Test
    fun example1() {
        GlobalScope.launch {
            delay(1000L)
            println("World!")
        }
        println("Hello,")
        Thread.sleep(2000L)
    }

    @Test
    fun example2() {
        thread {
            Thread.sleep(1000L)
            println("World!")
        }
        println("Hello,")
        Thread.sleep(2000L)
    }

    @Test
    fun example3() = runBlocking {
        delay(3000)
        println("Hello world!")
    }

    @Test
    fun example4() = runBlocking {
        launch {
            delay(1000L)
            println("World!")
        }
        println("Hello,")
        delay(2000L)
    }

    @Test
    fun example5() = runBlocking {
        val job = launch {
            delay(1000L)
            println("World!")
        }
        job.join()
        println("Hello,")
        delay(2000L)
    }

    @Test
    fun example7() = runBlocking {
        launch {
            delay(200L)
            println("Task from runBlocking")
        }

        coroutineScope {
            launch {
                delay(500L)
                println("Task from nested launch")
            }

            delay(100L)
            println("Task from coroutine scope")
        }

        println("Coroutine scope is over")
    }

    @Test
    fun example8() = runBlocking {
        performOperation()
    }

    @Test
    fun example9() = runBlocking {
        val time = measureTimeMillis {
            val one = testOperation1()
            val two = testOperation2()
            println("The answer is ${one + two}")
        }
        println("Execution time: $time")
    }

    @Test
    fun example10() = runBlocking {
        val time = measureTimeMillis {
            val one = async { testOperation1() }
            val two = async { testOperation2() }
            println("The answer is ${one.await() + two.await()}")
        }
        println("Execution time: $time")
    }

    @Test
    fun example11() = runBlocking {
        val one = async { testOperation1() }
        val two = async { errorOperation() }
        println("Result: ${one.await() + two.await()}")
    }

    @Test
    fun example12() = runBlocking {
        val result = coroutineScope {
            val one = async { testOperation1() }
            val two = async { errorOperation() }
            one.await() + two.await()
        }
        println("Result: $result")
    }

    @Test
    fun example13() = runBlocking {
        Executors.newSingleThreadExecutor().asCoroutineDispatcher()

        val one = async(Dispatchers.IO) { testOperation1() }
        val two = async(Dispatchers.IO) { testOperation2() }
        println("Result: ${one.await() + two.await()}")
    }

    @Test
    fun example14() = runBlocking {
        repeat(100_000) {
            launch {
                delay(1000L)
                print(".")
            }
        }
    }

    private suspend fun performOperation() {
        delay(1000)
        println("Operation performed")
    }

    private suspend fun testOperation1(): Int = try {
        delay(1000L) // pretend we are doing something useful here
        println("testOperation1 complete")
        13
    } finally {
        println("Test operation 1 finally")
    }

    private suspend fun testOperation2(): Int = try {
        delay(1000L) // pretend we are doing something useful here, too
        println("testOperation2 complete")
        29
    } finally {
        println("Test operation 2 finally")
    }

    private suspend fun errorOperation(): Int {
        delay(800)
        throw Exception("Test operation failed")
    }
}
